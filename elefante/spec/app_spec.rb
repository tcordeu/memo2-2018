# spec/app_spec.rb

require 'json'
require File.expand_path 'spec_helper.rb', __dir__

describe 'Elefante App' do
  it 'get /calc?cant=27&precio=76.6&estado=NV devuelve 2166.64632' do
    get '/calc?cant=27&precio=76.6&estado=NV'
    resultado = JSON.parse(last_response.body)['resultado']
    expect(last_response).to be_ok
    expect(resultado).to eq 2166.64632
  end
end
