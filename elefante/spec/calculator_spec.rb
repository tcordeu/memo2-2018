require 'rspec'
require_relative '../model/calculator'

describe 'Calculator' do
  let(:calculator) { Calculator.new }

  it 'calcular de cantidad 1, precio 100 con estado UT es 106.85' do
    expect(calculator.calc(1, 100, 'UT')).to eq 106.85
  end

  it 'calcular de cantidad 100, precio 2 con estado NV es 216' do
    expect(calculator.calc(100, 2, 'NV')).to eq 216
  end

  it 'calcular de cantidad 2, precio 750 con estado TX es 1545.9375' do
    expect(calculator.calc(2, 750, 'TX')).to eq 1545.9375
  end

  it 'calcular cantidad 105, precio 50 con estado AL es 5187' do
    expect(calculator.calc(105, 50, 'AL')).to eq 5187
  end

  it 'calcular cantidad 200, precio 1000 con estado CA es 184025' do
    expect(calculator.calc(200, 1000, 'CA')).to eq 184025
  end
end
