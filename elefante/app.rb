require 'sinatra'
require 'json'
require_relative '../elefante/model/calculator'

get '/calc' do
  calculator = Calculator.new
  cantidad = params['cant'].to_i
  precio = params['precio'].to_f
  estado = params['estado']

  content_type :json
  { resultado: calculator.calc(cantidad, precio, estado) }.to_json
end
