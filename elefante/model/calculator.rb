# Calcula el precio total de una cantidad de productos
# segun el estado ingresado.
class Calculator
  ESTADOS_IMPUESTO = { 'UT' => 0.0685, 'NV' => 0.08, 'TX' => 0.0625,
                       'AL' => 0.04, 'CA' => 0.0825 }.freeze

  DESCUENTOS = {
    0...1000 => 0,
    1000...5000 => 0.03,
    5000...7000 => 0.05,
    7000...10_000 => 0.07,
    10_000...50_000 => 0.10,
    50_000...Float::INFINITY => 0.15
  }.freeze
  def calc(cantidad, precio_unit, estado)
    aplicar_impuesto(aplicar_descuento(cantidad * precio_unit), estado)
  end

  private

  def aplicar_descuento(total)
    total * (1 - DESCUENTOS.detect { |rango, _v| rango.include?(total) }.last)
  end

  def aplicar_impuesto(total, estado)
    total * (1 + ESTADOS_IMPUESTO[estado])
  end
end
