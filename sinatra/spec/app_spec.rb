# spec/app_spec.rb

require File.expand_path 'spec_helper.rb', __dir__

describe 'Sinatra Application' do
  it 'get /sum?x=9,9 deberia devolver "uno,ocho" y "9,9"' do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include '9,9'
    expect(last_response.body).to include 'uno,ocho'
  end

  it 'post /chop con x=3 e y=0,7,3' do
    post '/chop', 'x' => '3', 'y' => '0,7,3'
    expect(last_response).to be_ok
    expect(last_response.body).to include '2'
    expect(last_response.body).to include 'x=3,y=0,7,3'
  end
end