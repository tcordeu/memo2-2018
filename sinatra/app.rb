require 'sinatra'
require 'json'
require_relative '../sinatra/model/chopper'

get '/sum' do
  chopper = Chopper.new
  operacion = params['x']
  operando_a = operacion.split(',')[0].to_i
  operando_b = operacion.split(',')[1].to_i

  content_type :json
  { resultado: chopper.sum([operando_a, operando_b]), sum: operacion }.to_json
end

post '/chop' do
  chopper = Chopper.new
  operandos_y = params['y']
  indice = params['x'].to_i
  array_y = operandos_y.split(',').map!(&:to_i)

  content_type :json
  { resultado: chopper.chop(indice, array_y),
    chop: "x=#{indice},y=#{operandos_y}" }.to_json
end