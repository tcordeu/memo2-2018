class Chopper

  DIGITS = {
    '0' => 'cero',
    '1' => 'uno',
    '2' => 'dos',
    '3' => 'tres',
    '4' => 'cuatro',
    '5' => 'cinco',
    '6' => 'seis',
    '7' => 'siete',
    '8' => 'ocho',
    '9' => 'nueve',
  }

  def chop(n, array)
    # Devuelve el indice del numero n en el array.
    # - Si el numero n no esta presente se devolvera -1.
    # Ej: chop(4, [0,4,3,1]) => 1.
    array.each_with_index do |number, index|
      if number == n
        return index
      end
    end

    return -1
  end

  def sum(array)
    # Devuelve un string numerando los digitos de la suma de todos los elementos del array.
    # - Si el array esta vacio devuelve 'vacio'.
    # - Si la suma es mayor o igual a 100 devuelve 'demasiado grande'.
    # Ej: sum([20,1]) => 'dos,uno'. 
    aux_array = []
    sum = array.reduce(:+)

    if sum.nil?
      return 'vacio'
    elsif sum >= 100
      return 'demasiado grande'
    else
      sum.to_s.split('').each do |char|
        aux_array.push(DIGITS[char])
      end
      return aux_array.join(',')
    end
  end

end
