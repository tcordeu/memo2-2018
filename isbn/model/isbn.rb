class Isbn
  MODULE_ISBN = 11
  CHARS_ADICIONALES = [' ', '-']
  CHARS_VALIDOS = '0123456789X' + CHARS_ADICIONALES.join('')

  def compare_mod(str, sum)
    # Devuelve true si el modulo de la suma coincide con el ultimo
    # char y false en caso contrario.
    # PRE: La string debe tener 10 caracteres.
    # PRE: La string no contiene chars invalidos.
    mod = sum % MODULE_ISBN

    if str[-1] == 'X' and mod == 10
      return true
    end
    return mod == str[-1].to_i
  end

  def check_sum(str)
    # Devuelve true si el ultimo digito coincide con lo especificado por ISBN-10
    # y false en caso contrario.
    # PRE: La string debe tener 10 caracteres.
    # PRE: La string no contiene chars invalidos.
    aux_list = str.split("").map{|x| x.to_i}
    last_index = aux_list.length - 1
    sum = 0

    aux_list[0, last_index].each_index do |index|
      sum += (aux_list[index] * (index + 1))
    end

    return compare_mod(str, sum)
  end

  def remove_extra_chars(str)
    # Devuelve una string sin caracteres permitidos adicionales.
    # POST: No modifica la string recibida.
    # Ej: Isbn.remove_extra_chars('0 471 95869 7') => '0471958697'.
    result = str.dup

    CHARS_ADICIONALES.each do |char|
      result = result.tr(char, '')
    end
      
    return result
  end

  def check_valid_chars(str)
    # Devuelve true si la string solamente contiene chars validos
    # y false en caso contrario.
    # PRE: Los chars validos son: numeros, chars adicionales y 'X'.
    str.split("").each do |char|
      unless CHARS_VALIDOS.include?(char)
        return false
      end
    end
    return true
  end

  def is_valid(str)
    # Devuelve true si la string recibida es valida segun ISBN-10
    # y false en caso contrario.
    aux_str = remove_extra_chars(str)
    if aux_str.length == 10 and check_valid_chars(aux_str) and check_sum(aux_str)
      return true
    end

    return false
  end

end
