require 'rspec'
require_relative '../model/isbn'

describe 'Isbn' do
  
  let(:isbn) {Isbn.new}

  it 'is_valid de 0471958697 deberia ser true' do
    expect(isbn.is_valid("0471958697")).to eq true
  end

  it 'is_valid de 0 471 95869 7 deberia ser true' do
    expect(isbn.is_valid("0 471 95869 7")).to eq true
  end

  it 'is_valid de 0-471-95869-7 deberia ser true' do
    expect(isbn.is_valid("0-471-95869-7")).to eq true
  end

  it 'is_valid de 0-471 95-869 7 deberia ser true' do
    expect(isbn.is_valid("0-471 95-869 7")).to eq true
  end

  it 'is_valid de 155404295X deberia ser true' do
    expect(isbn.is_valid("155404295X")).to eq true
  end

  it 'is_valid de 155-404 2 95X deberia ser true' do
    expect(isbn.is_valid("155-404 2 95X")).to eq true
  end

  it 'is_valid de 0471958698 deberia ser false' do
    expect(isbn.is_valid("0471958698")).to eq false
  end

  it 'is_valid de 047195 deberia ser false' do
    expect(isbn.is_valid("047195")).to eq false
  end

  it 'is_valid de a471958697 deberia ser false' do
    expect(isbn.is_valid("a471958697")).to eq false
  end

  it 'is_valid de 0471958a9a deberia ser false' do
    expect(isbn.is_valid("0471958a9a")).to eq false
  end
  
end
